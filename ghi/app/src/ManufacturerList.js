import React, { useState, useEffect } from "react"
import { Link } from "react-router-dom";

function ManufacturerList({manufacturers, getManufacturer}) {
    const deleteManufacturer = async (manufacturer) => {
      const manufacturerUrl = `http://localhost:8100/api/manufacturers/${manufacturer.id}/`
      const fetchConfig = {
        method: 'delete'
      };
    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
        getManufacturer();

    }
    }


    return (
        <>
        <div className="mt-3">
        <h1>Manufacturers</h1>
            <table className="table table-striped table-hover align-middle mt-5">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map((manufacturer) => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            </div>
        </>
    );
}

export default ManufacturerList;
