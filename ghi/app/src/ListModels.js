import React, { useEffect, useState } from 'react';
import './index.css';

function ModelsList(props) {
	console.log(props.models);
	return (
		<div className="mt-3">
        <h1>Vehicle Models</h1>
		<table className="table table-striped">
			<thead>
				<tr>
					<th>Model Name</th>
					<th>Manufacturer</th>
					<th>Picture</th>
				</tr>
			</thead>
			<tbody>
				{props.models.map((model) => {
					return (
						<tr key={model.id}>
							<td>{model.name}</td>
							<td>{model.manufacturer.name}</td>
							<td>
								{' '}
								<img
									className="modelsize"
									src={model.picture_url}
								/>{' '}
							</td>
						</tr>
					);
				})}
			</tbody>
		</table>
		</div>
	);
}

export default ModelsList;
