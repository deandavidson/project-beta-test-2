from django.urls import path
from .views import list_services, show_service, create_tech, show_tech

urlpatterns = [
    path("techs/", create_tech, name="create_tech"),
    path("techs/<int:id>/", show_tech, name="show_tech"),
    path("services/", list_services, name="list_services"),
    path("services/<int:id>/", show_service, name="show_service"),
]
